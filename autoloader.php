<?php

spl_autoload_register(function ($class) {
    // Convert namespace separators to directory separators
    $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);

    // Load class files based on PSR-4 autoloading standard
    $prefix = 'EncryptionExample\\';
    $baseDir = __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;

    if (strpos($class, $prefix) === 0) {
        $class = substr($class, strlen($prefix));
        $file = $baseDir . $class . '.php';

        if (file_exists($file)) {
            require $file;
        }
    }
});

# Encryption Example

This is a simple PHP encryption example that demonstrates how to encrypt and decrypt data using different encryption methods.

## Prerequisites

- PHP version 7.0 or higher

## Getting Started

1. Clone the repository or download the code files.

2. Include the autoloader file in your PHP script:

   ```php
   require 'autoloader.php';
   ```

3. Create an instance of the desired encryption method using the EncryptionFactory class.

   ```php
   $key = "my_secret_key"; // example - $#GDSGYHFGH
   $method = "AES";
   $encryption = EncryptionFactory::createEncryption($method, $key);
   ```

   You can choose between different encryption methods such as AES or DES by setting the $method variable accordingly. Replace $key with your desired encryption key.

4. Encrypt and decrypt data using the created encryption instance.

   ```php
   $data = "Secret text to be encrypted";
   $encryptedData = $encryption->encrypt($data);
   echo "Encrypted data: " . $encryptedData . "<br />";

   $decryptedData = $encryption->decrypt($encryptedData);
   echo "Decrypted data: " . $decryptedData . "<br />";
   ```

   Replace $data with the data you want to encrypt and decrypt.

## Encryption Methods

The code includes the following encryption methods:

AES: Advanced Encryption Standard, using AES-256-CBC encryption algorithm.
DES: Data Encryption Standard, using DES-CBC encryption algorithm.
You can add more encryption methods by implementing the EncryptionMethod interface and adding them to the EncryptionFactory class.

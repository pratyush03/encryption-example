<?php

require 'autoloader.php';

use EncryptionExample\EncryptionFactory;

// Set the encryption key and method
$key = "my_secret_key";
$method = "AES";

// Create an instance of the desired encryption method
$encryption = EncryptionFactory::createEncryption($method, $key);

$data = "Secret text";

// Encrypt the data
$encryptedData = $encryption->encrypt($data);


echo "Encrypted data: " . $encryptedData . "<br />";

// Decrypt the data
$decryptedData = $encryption->decrypt($encryptedData);

// Display the decrypted data
echo "Decrypted data: " . $decryptedData . "<br />";

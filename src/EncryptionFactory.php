<?php

namespace EncryptionExample;

/**
 * Encryption factory responsible for creating EncryptionMethod instances.
 */
class EncryptionFactory
{
    /**
     * Creates an instance of the specified encryption method.
     *
     * @param string $method The encryption method.
     * @param string $key The encryption key.
     *
     * @return EncryptionMethod An instance of the specified encryption method.
     *
     * @throws \Exception If an unsupported encryption method is requested.
     */
    public static function createEncryption($method, $key)
    {
        switch ($method) {
            case 'AES':
                return new AESEncryption($key);
            case 'DES':
                return new DESEncryption($key);
            // Add more encryption methods here if required
            default:
                throw new \Exception("Unsupported encryption method: $method");
        }
    }
}

<?php

namespace EncryptionExample;

/**
 * Interface defining the EncryptionMethod contract.
 */
interface EncryptionMethod
{
    /**
     * Encrypts the provided data.
     *
     * @param string $data The data to encrypt.
     *
     * @return string The encrypted data.
     */
    public function encrypt($data);

    /**
     * Decrypts the provided encrypted data.
     *
     * @param string $encryptedData The encrypted data to decrypt.
     *
     * @return string The decrypted data.
     */
    public function decrypt($encryptedData);
}

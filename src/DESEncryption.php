<?php

namespace EncryptionExample;

/**
 * DES encryption implementation of the EncryptionMethod interface.
 */
class DESEncryption implements EncryptionMethod
{
    private $key;
    
    /**
     * DESEncryption constructor.
     *
     * @param string $key The encryption key.
     */
    public function __construct($key)
    {
        $this->key = $key;
    }
    
    /**
     * Encrypts the provided data using DES encryption.
     *
     * @param string $data The data to encrypt.
     *
     * @return string The encrypted data.
     */
    public function encrypt($data)
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('DES-CBC'));
        $encrypted = openssl_encrypt($data, 'DES-CBC', $this->key, 0, $iv);
        return base64_encode($iv . $encrypted);
    }
    
    /**
     * Decrypts the provided encrypted data using DES decryption.
     *
     * @param string $encryptedData The encrypted data to decrypt.
     *
     * @return string The decrypted data.
     */
    public function decrypt($encryptedData)
    {
        $data = base64_decode($encryptedData);
        $ivLength = openssl_cipher_iv_length('DES-CBC');
        $iv = substr($data, 0, $ivLength);
        $encrypted = substr($data, $ivLength);
        return openssl_decrypt($encrypted, 'DES-CBC', $this->key, 0, $iv);
    }
}
